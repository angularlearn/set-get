import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'child-comp',
  templateUrl: './child-comp.component.html',
  styleUrls: ['./child-comp.component.css']
})
export class ChildCompComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }


  @Input() userName: string;
  _userAge: number;

  @Input()
  set userAge(age:number) {
      if(age < 0)
          this._userAge = 18;
      else if(age > 100)
          this._userAge = 100;
      else
          this._userAge = age;
  }
  get userAge() { return this._userAge; }
}
